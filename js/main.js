"use strict";

// https://superheroapi.com/api/10222724375568479

window.addEventListener('DOMContentLoaded', function () {
    // console.log("shake and bake");
    // displayMenuItems(menu);
    // displayMenuButtons();
});

const container = document.querySelector(".btn-container");
var ajaxRequest = $.ajax("data/superheroes.json", {
}).done(function (data) {
    console.log(data);
    // console.log(data.biography.publisher);

    var dataHTML = getAllSuperheroes(data);
    // console.log(data.length);

    $("#insertCharacters").html(dataHTML);

});

// CATCH A SUBMIT AND TAKE THE VALUE





// function displaySuperItems(superItems) {
//
//     var displaySuperheroes = superItems.map(function (item) {
//         // console.log(item);
//         return `<div class="menu-item">
//              <img src="${item.img}" class="photo" alt="">
//               <div class="item-info">
//                <header>
//                 <h4>${item.title}</h4>
//                 <h4 class="price">${item.price}</h4>
//                </header>
//                 <p class="item-text">${item.desc}</p>
//               </div>
//             </div>`;
//
//     });
//     // console.log(displayMenu);
//     displayData() = displayData.join("");
//     sectionCenter.innerHTML = displayMenu;
//
// }


// function displayData(data) {
//
//     var displayOnHTML = '';
//
//     // for (var i = 0; i <= 10; i++) {
//     //
//     //     displayOnHTML +=
//     //         `
//     //         <h3>${data.name}</h3>
//     //         `
//     // }
//
//     data.forEach(function (person) {
//
//         displayOnHTML +=
//             `
//             <div class="col-sm-6 col-lg-4 pb-5 text-center">
//                 <div class="card">
//                     <img class="card-img-top" id="person-image" src="${person.images.md}" alt="Card image cap">
//                     <div class="card-body">
//                         <p class="card-text">${person.name}</p>
//                     </div>
//                 </div>
//             </div>
//
//
//             `
//
//
//     });
//     return displayOnHTML;
//
// }

// function displayMenuButtons() {
//     var categories = ajaxRequest.map(function (value, item) {
//         if (!value.includes(item.biography.publisher)) {
//             value.push(item.biography.publisher);
//         }
//
//         return value;
//
//     }, ['all']);
//
//
//     var categoryBtns = categories.map(function (category) {
//
//         return `
//
//     <button type="button" class="btn btn-primary" data-id="${category}">${category}</button>
//
// `
//
//     })
//         .join("");
//     container.innerHTML = categoryBtns;
//     const filterBtns = container.querySelectorAll('.filter-btn');
//     // filter items
//     filterBtns.forEach(function (btn) {
//
//         btn.addEventListener('click', function (e) {
//
//             // console.log(e.currentTarget.dataset.id);
//             var category = e.currentTarget.dataset.id;
//
//             var menuCategory = ajaxRequest.filter(function (superItem) {
//                 // console.log(menuItem.category);
//
//                 if (superItem.category === category) {
//                     // console.log(menuItem);
//                     return superItem;
//
//                 }
//
//             });
//             // console.log(menuCategory);
//
//             if (category === 'all') {
//                 displayData(menu)
//             }
//             else {
//                 displayData(menuCategory);
//             }
//
//         })
//
//     });
//     // console.log(categories);
//
// }



// function  displayData(data) {
//
//     var usersOnHTML = '';
//
//     data.forEach(function (d) {
//         if (d.biography.publisher === 'DC Comics') {
//             usersOnHTML +=
//                 `
//         <div class="col-sm-6 col-lg-4 pb-5 text-center">
//                 <div class="card">
//                     <img class="card-img-top" id="person-image" src="${d.images.md}" alt="Card image cap">
//                     <div class="card-body">
//                         <p class="card-text">${d.name}</p>
//                     </div>
//                 </div>
//             </div>
//         `
//         }
//
//     });
//
//     return usersOnHTML;
//
// }

function displayDC(data) {

    var usersOnHTML = '';

    data.forEach(function (d) {
        if (d.biography.publisher === 'DC Comics') {
            usersOnHTML +=
                `
        <div class="col-sm-6 col-lg-4 pb-5 text-center">
                <div class="card">
                    <img class="card-img-top" id="person-image" src="${d.images.md}" alt="Card image cap">
                    <div class="card-body">
                        <p class="card-text">${d.name}</p>
                    </div>
                </div>
            </div>
        `
        }

    });

    return usersOnHTML;

}

function  displayMarvel(data) {

    var usersOnHTML = '';

    data.forEach(function (d) {
        if (d.biography.publisher === 'Marvel Comics') {
            usersOnHTML +=
                `
        <div class="col-sm-6 col-lg-4 pb-5 text-center">
                <div class="card">
                    <img class="card-img-top" id="person-image" src="${d.images.md}" alt="Card image cap">
                    <div class="card-body">
                        <p class="card-text">${d.name}</p>
                    </div>
                </div>
            </div>
        `
        }

    });

    return usersOnHTML;

}

function  displayDarkHorse(data) {

    var usersOnHTML = '';

    data.forEach(function (d) {
        if (d.biography.publisher === 'Dark Horse Comics') {
            usersOnHTML +=
                `
        <div class="col-sm-6 col-lg-4 pb-5 text-center">
                <div class="card">
                    <img class="card-img-top" id="person-image" src="${d.images.md}" alt="Card image cap">
                    <div class="card-body">
                        <p class="card-text">${d.name}</p>
                    </div>
                </div>
            </div>
        `
        }

    });

    return usersOnHTML;

}

// function  displayData(data) {
//
//     var usersOnHTML = '';
//
//     data.forEach(function (d) {
//         if (d.biography.publisher !== 'Dark Horse Comics' && d.biography.publisher !== 'DC Comics' && d.biography.publisher !== 'Marvel Comics') {
//             usersOnHTML +=
//                 `
//         <div class="col-sm-6 col-lg-4 pb-5 text-center">
//                 <div class="card">
//                     <img class="card-img-top" id="person-image" src="${d.images.md}" alt="Card image cap">
//                     <div class="card-body">
//                         <p class="card-text">${d.name}</p>
//                     </div>
//                 </div>
//             </div>
//         `
//         }
//
//     });
//
//     return usersOnHTML;
//
// }


function  getAllSuperheroes(data) {

    var usersOnHTML = '';

    data.forEach(function (d) {
        // if (d.name === 'BatmanII') {
            usersOnHTML +=
                `
        <div class="col-sm-6 col-lg-4 pb-5 text-center">
                <div class="card">
                    <img class="card-img-top" id="person-image" src="${d.images.md}" alt="Card image cap">
                    <div class="card-body">
                        <p class="card-text">${d.name}</p>
                            <a onclick="superheroSelected('${d.name}')" href="#" class="btn btn-primary">Go somewhere</a>

                    </div>
                </div>
            </div>
        `
        // }

    });

    return usersOnHTML;

}

function superheroSelected(name) {
    sessionStorage.setItem('name', name);
    window.location = "hero-info.html";
    return false;
}


// after click more info btn
function getSuperhero(){
    var superheroName = sessionStorage.getItem('name');

    $.ajax('data/superheroes.json', {

    }).done(function(data) {
            console.log(data);
            console.log(superheroName);
            var hero = superheroName;
            // var image = data.images.md;

            var output = '';

            data.forEach(function (user){
                if ( user.name === superheroName) {
               output = `
                    <div class="row">
                        <div class="col-md-4">
                            <img src="${user.images.md}" class="thumbnail">
                        </div>

                        <div class="col-md-8">
                            <h2 class="text-light">${hero}</h2>
                            <ul class="list-group">
                                <li class="list-group-item"><strong>Full Name:</strong> ${user.biography.fullName}</li>
                                <li class="list-group-item"><strong>Alter Ego:</strong> ${user.biography.alterEgos}</li>
                                <li class="list-group-item"><strong>Occupation:</strong> ${user.work.occupation}</li>

                                <li class="list-group-item"><strong>Hometown:</strong> ${user.biography.placeOfBirth}</li>
    
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 pt-3">        
                            <a href="index.html" role="button" class="btn btn-primary">Go Back To Search</a>
                        </div>
                    </div>`
                }
        });


            $('#superhero').html(output);
        })
        .catch((err) => {
            console.log(err);
        });
}






